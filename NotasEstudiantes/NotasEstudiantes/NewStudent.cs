﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotasEstudiantes
{
    public partial class NewStudent : Form
    {
        public NewStudent()
        {
            InitializeComponent();
        }
        public DataSet dsNewStudent { get; set; }
        public bool editable { get; set; }
        public int id { get; set; }
        public Estudiante estu { get; set; }
        
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (editable==true)
            {
                ImplementData impl = new ImplementData();
                impl.Update(new Estudiante(Int32.Parse(mkdId.Text), txtCarnet.Text, txtNombres.Text, txtApellidos.Text,
                    mkdBimestrel.Text, mkdBimestrell.Text, mkdNotaFinal.Text, txtEstatus.Text));
                DataRow r = dsNewStudent.Tables[0].NewRow();
                r[0] = Int32.Parse(mkdId.Text);
                r[1] = txtCarnet.Text;
                r[2] = txtNombres.Text;
                r[3] = txtApellidos.Text;
                r[4] = mkdBimestrel.Text;
                r[5] = mkdBimestrell.Text;
                r[6] = mkdNotaFinal.Text;
                r[7] = txtEstatus.Text;
                dsNewStudent.Tables[0].Rows.RemoveAt(Int32.Parse(mkdId.Text)-1);
                dsNewStudent.Tables[0].Rows.InsertAt(r,Int32.Parse(mkdId.Text)-1);
            }
            else
            {                
                ImplementData impl = new ImplementData();
                impl.Create(new Estudiante(Int32.Parse(mkdId.Text), txtCarnet.Text, txtNombres.Text, txtApellidos.Text,
                    mkdBimestrel.Text, mkdBimestrell.Text, mkdNotaFinal.Text, txtEstatus.Text));                
                DataRow r = dsNewStudent.Tables[0].NewRow();
                r[0] = Int32.Parse(mkdId.Text);
                r[1] = txtCarnet.Text;
                r[2] = txtNombres.Text;
                r[3] = txtApellidos.Text;
                r[4] = mkdBimestrel.Text;
                r[5] = mkdBimestrell.Text;
                r[6] = mkdNotaFinal.Text;
                r[7] = txtEstatus.Text;
                dsNewStudent.Tables[0].Rows.Add(r);
            }           
            Dispose();
        }

        private void NewStudent_Load(object sender, EventArgs e)
        {
            if (editable == true)
            {
                mkdId.Text = estu.Id.ToString();
                mkdId.ReadOnly = true;
                txtCarnet.Text = estu.Carnet;
                txtCarnet.ReadOnly = true;
                txtNombres.Text = estu.Nombres;
                txtApellidos.Text = estu.Apellidos;
                mkdBimestrel.Text = estu.Bimestrel;
                mkdBimestrell.Text = estu.Bimestrell;
                mkdNotaFinal.Text = estu.NotaFinal;
                txtEstatus.Text = estu.Estatus;

            }
            else
            {
                mkdId.Text = id.ToString();
                mkdId.ReadOnly = true;
            }
        }
    }
}
