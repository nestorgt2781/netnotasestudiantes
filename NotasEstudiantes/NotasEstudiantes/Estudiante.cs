﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotasEstudiantes
{
    public class Estudiante
    {
        public int Id { get; set; }//10
        public string Carnet { get; set; }//40
        public string Nombres { get; set; }//60
        public string Apellidos { get; set; }//60
        public string Bimestrel { get; set; }//10
        public string Bimestrell { get; set; }//10
        public string NotaFinal { get; set; }//10
        public string Estatus { get; set; }//40

        public Estudiante() { }

        public Estudiante(int id, string carnet, string nombres, string apellidos, string bimestrel, string bimestrell, string notaFinal, string estatus)
        {
            Id = id;
            Carnet = carnet;
            Nombres = nombres;
            Apellidos = apellidos;
            Bimestrel = bimestrel;
            Bimestrell = bimestrell;
            NotaFinal = notaFinal;
            Estatus = estatus;
        }
    }
}
