﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotasEstudiantes
{
    public partial class Form1 : Form
    {
        public BindingSource bindingSource;
        public Form1()
        {
            InitializeComponent();
            bindingSource = new BindingSource();
        }  
        private void Form1_Load(object sender, EventArgs e)
        {
            bindingSource.DataSource = dsMateria;
            bindingSource.DataMember = dsMateria.Tables[0].TableName;

            ImplementData imp = new ImplementData();
            List<Estudiante> estudiantes = imp.All();
            foreach (Estudiante t in estudiantes)
            {
                DataRow r = dsMateria.Tables[0].NewRow();
                r[0] = t.Id;
                r[1] = t.Carnet;
                r[2] = t.Nombres;
                r[3] = t.Apellidos;
                r[4] = t.Bimestrel;
                r[5] = t.Bimestrell;
                r[6] = t.NotaFinal;
                r[7] = t.Estatus;
                dsMateria.Tables[0].Rows.Add(r);
            }
            dgvStudents.DataSource = bindingSource;
            dgvStudents.AutoGenerateColumns = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            NewStudent student = new NewStudent();
            student.editable = false;
            student.id = dsMateria.Tables[0].Rows.Count+1;
            student.dsNewStudent = dsMateria;
            student.ShowDialog(this);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            NewStudent student = new NewStudent();
            student.editable = true;
            student.dsNewStudent = dsMateria;
            DataRow dataRow = dsMateria.Tables[0].Rows[dgvStudents.CurrentRow.Index];
            student.estu = new Estudiante(Int32.Parse(dataRow[0].ToString()),dataRow[1].ToString(), dataRow[2].ToString(),
                dataRow[3].ToString(), dataRow[4].ToString(), dataRow[5].ToString(), dataRow[6].ToString(), dataRow[7].ToString());
            student.ShowDialog(this);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
            DataRow dataRow = dsMateria.Tables[0].Rows[dgvStudents.CurrentRow.Index];
            ImplementData impl = new ImplementData();
            impl.Delete(new Estudiante(Int32.Parse(dataRow[0].ToString()), dataRow[1].ToString(), dataRow[2].ToString(),
            dataRow[3].ToString(), dataRow[4].ToString(), dataRow[5].ToString(), dataRow[6].ToString(), dataRow[7].ToString()));
            dsMateria.Tables[0].Rows.RemoveAt(dgvStudents.CurrentRow.Index);
        }

        private void btnReprobados_Click(object sender, EventArgs e)
        {
            
            if (btnReprobados.Text.Equals("Reprobados"))
            {
                btnAprovados.Text = "General";
                btnReprobados.Text = "General";
                for (int i = 0;i<dgvStudents.RowCount-1;i++)
                {
                    DataRow dr = dsMateria.Tables[0].Rows[i];
                    if (Int32.Parse(dr[6].ToString())>=60 && dgvStudents.CurrentRow.Index!=i)
                    {
                        dgvStudents.Rows[i].Visible = false;
                    }
                }
            }
            else
            {
                btnReprobados.Text = "Reprobados";
                btnAprovados.Text = "Aprobados";
                for (int i = 0; i < dgvStudents.RowCount-1; i++)
                {
                    dgvStudents.Rows[i].Visible = true;
                }
            }            
        }

        private void btnAprovados_Click(object sender, EventArgs e)
        {
           
            if (btnAprovados.Text.Equals("Aprobados"))
            {
                btnAprovados.Text = "General";
                btnReprobados.Text= "General";
                for (int i = 0; i < dgvStudents.RowCount - 1; i++)
                {
                    DataRow dr = dsMateria.Tables[0].Rows[i];
                    if (Int32.Parse(dr[6].ToString())< 60 && dgvStudents.CurrentRow.Index != i)
                    {
                        dgvStudents.Rows[i].Visible = false;
                    }
                }
            }
            else
            {
                btnAprovados.Text = "Aprobados";
                btnReprobados.Text = "Reprobados";
                for (int i = 0; i < dgvStudents.RowCount - 1; i++)
                {
                    dgvStudents.Rows[i].Visible = true;
                }
            }
        }
    }
}
