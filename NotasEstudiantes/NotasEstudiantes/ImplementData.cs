﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotasEstudiantes
{
    class ImplementData
    {

        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 240;

        public ImplementData() { }

        private void Open()
        {
            try
            {
                fsHeader = new FileStream("hestudiante.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("destudiante.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//n
                }


            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        } 
        public void Create(Estudiante t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                long pos = n * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++n);
                bwData.Write(t.Carnet);
                bwData.Write(t.Nombres);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Bimestrel);
                bwData.Write(t.Bimestrell);
                bwData.Write(t.NotaFinal);
                bwData.Write(t.Estatus);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(n);
                
                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public void Update(Estudiante t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                for (int i=0; i<n; i++)
                {
                    int pos = i * SIZE;
                    brData.BaseStream.Seek(pos, SeekOrigin.Begin);
                    brData.ReadInt32();
                    if (t.Carnet.Equals(brData.ReadString()))
                    {
                        bwData.BaseStream.Seek(pos, SeekOrigin.Begin);                        
                        bwData.Write(t.Id-1);
                        bwData.Write(t.Carnet);
                        bwData.Write(t.Nombres);
                        bwData.Write(t.Apellidos);
                        bwData.Write(t.Bimestrel);
                        bwData.Write(t.Bimestrell);
                        bwData.Write(t.NotaFinal);
                        bwData.Write(t.Estatus);                        
                    }
                }
                
                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public void Delete(Estudiante estd)
        {
            List<Estudiante> estudiantes = All();
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                int i = 0;
                foreach (Estudiante t in estudiantes)
                {
                    int pos = i * SIZE;
                    brData.BaseStream.Seek(pos, SeekOrigin.Begin);
                    if (brData.ReadInt32()!=t.Id)
                    {
                        pos = i * SIZE;
                        bwData.BaseStream.Seek(pos, SeekOrigin.Begin);
                        bwData.Write(t.Id - 1);
                        bwData.Write(t.Carnet);
                        bwData.Write(t.Nombres);
                        bwData.Write(t.Apellidos);
                        bwData.Write(t.Bimestrel);
                        bwData.Write(t.Bimestrell);
                        bwData.Write(t.NotaFinal);
                        bwData.Write(t.Estatus);
                        i++;
                    }                    
                  
                }
                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(--n);
                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public List<Estudiante> All()
        {
            List<Estudiante> estudiantes = new List<Estudiante>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                Estudiante estudiante = null;
                if (n!=0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        long dpos = (i) * SIZE;
                        brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                        estudiante = new Estudiante()
                        {
                            Id = brData.ReadInt32(),
                            Carnet = brData.ReadString(),
                            Nombres = brData.ReadString(),
                            Apellidos = brData.ReadString(),
                            Bimestrel = brData.ReadString(),
                            Bimestrell = brData.ReadString(),
                            NotaFinal = brData.ReadString(),
                            Estatus = brData.ReadString()
                        };

                        estudiantes.Add(estudiante);
                    }
                }    
                
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return estudiantes;
        }
    }
}
